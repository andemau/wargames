package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Battle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {
    Army armyOne;
    Army armyTwo;
    Battle battle;

    @BeforeEach
    void setUp() {
        armyOne = new Army();
        armyOne.generateArmy();
        armyTwo = new Army();
        armyTwo.generateArmy();
        battle = new Battle(armyOne, armyTwo);
    }

    @Nested
    class AttackBonusTest {
        @Nested
        class Positive {
            @Test
            void attackBonusWithHillTerrain() {
                battle.setTerrain(Battle.Terrain.HILL);
                assertEquals(7, armyOne.getRangedUnits().get(0).getAttackBonus(), "Attack bonus should be 7 for hill terrain");
            }
            @Test
            void attackBonusWithForestTerrain() {
                battle.setTerrain(Battle.Terrain.FOREST);
                assertEquals(5, armyOne.getRangedUnits().get(0).getAttackBonus(), "Attack bonus should be 5 for forest terrain");
            }
            @Test
            void attackBonusWithMountainTerrain() {
                battle.setTerrain(Battle.Terrain.PLAINS);
                assertEquals(3, armyOne.getRangedUnits().get(0).getAttackBonus(), "Attack bonus should be 3 for mountain terrain");
            }
        }
    }

    @Nested
    class ResistBonusTest {
        @Nested
        class Positive {
            @Test
            void resistBonusWithThreeRange() {
                assertEquals(6, armyOne.getRangedUnits().get(0).getResistBonus(), "Resist bonus should be 6 for three range");
            }
            @Test
            void resistBonusWithTwoRange() {
                armyOne.getRangedUnits().get(0).attack(armyTwo.getRangedUnits().get(0));
                assertEquals(4, armyOne.getRangedUnits().get(0).getResistBonus(), "Resist bonus should be 4 for two range");
            }
            @Test
            void resistBonusWithOneRange() {
                armyOne.getRangedUnits().get(0).attack(armyTwo.getRangedUnits().get(0));
                armyOne.getRangedUnits().get(0).attack(armyTwo.getRangedUnits().get(0));
                assertEquals(2, armyOne.getRangedUnits().get(0).getResistBonus(), "Resist bonus should be 2 for one range");
            }
        }
    }
}