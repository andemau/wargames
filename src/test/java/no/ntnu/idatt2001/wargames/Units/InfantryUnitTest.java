package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Battle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {
    Army armyOne;
    Army armyTwo;
    Battle battle;

    @BeforeEach
    void setUp() {
        armyOne = new Army();
        armyOne.generateArmy();
        armyTwo = new Army();
        armyTwo.generateArmy();
        battle = new Battle(armyOne, armyTwo);
    }

    @Nested
    class AttackBonusTest {
        @Nested
        class Positive {
            @Test
            void attackBonusWithForestTerrain() {
                battle.setTerrain(Battle.Terrain.FOREST);
                assertEquals(5, armyOne.getInfantryUnits().get(0).getAttackBonus(), "Attack bonus should be 5 with forest terrain");
            }
            @Test
            void attackBonusWithHillTerrain() {
                battle.setTerrain(Battle.Terrain.HILL);
                assertEquals(2, armyOne.getInfantryUnits().get(0).getAttackBonus(), "Attack bonus should be 2 with hill terrain");
            }
        }
    }

    @Nested
    class DefenseBonusTest {
        @Nested
        class Positive {
            @Test
            void resistBonusWithForestTerrain() {
                battle.setTerrain(Battle.Terrain.FOREST);
                assertEquals(5, armyOne.getInfantryUnits().get(0).getResistBonus(), "Defense bonus should be 5 with forest terrain");
            }
        }
    }
}