package no.ntnu.idatt2001.wargames.Units;

import org.junit.jupiter.api.*;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Information on TestFactory and dynamic testing found at:
 * https://www.baeldung.com/junit5-dynamic-tests
 *
 * Uses Lists with values for input and expected output to automate testing.
 *
 * Returns a stream of test cases, where the test cases are created by iterating through
 * the input value list and checking if the provided input values result in the expected
 * output values, by creating defender units with the provided input values and attacking
 * them with a default attacker unit.
 */
@IndicativeSentencesGeneration(separator = " -> ", generator =
        DisplayNameGenerator.ReplaceUnderscores.class)

class UnitTest {
    @Nested
    class Attack {
        @Nested
        class Positive {
            @TestFactory
            Stream<DynamicTest> Units_attacked_with_different_health_values() {

                List<String> inputHealthValues = Arrays.asList(
                        "0", "1", "10", "100", "1000");
                List<String> outputList = Arrays.asList(
                        "0", "0", "0", "85", "985");

                return inputHealthValues.stream()
                        .map(healthValue -> DynamicTest.dynamicTest("Testing with health input:  " + healthValue,
                                () -> {
                                    int id = inputHealthValues.indexOf(healthValue);

                                    Unit defender = new InfantryUnit("Defender", Integer.parseInt(healthValue));
                                    Unit attacker = new CavalryUnit("Attacker", 20);
                                    attacker.attack(defender);

                                    assertEquals(outputList.get(id), String.valueOf(defender.getHealth()));
                                }));
            }
        }
    }

    @Nested
    class Constructor {
        @Nested
        class Negative {
            @Test
            void Unit_can_not_be_created_with_negative_health() {
                assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Unit", -1));
            }
            @Test
            void Unit_can_not_be_created_with_empty_name() {
                assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("", 1));
            }
            @Test
            void Unit_can_not_be_created_with_null_name() {
                assertThrows(IllegalArgumentException.class, () -> new InfantryUnit(null, 1));
            }
        }
    }
}
