package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Battle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {
    Army armyOne;
    Army armyTwo;
    Battle battle;

    @BeforeEach
    void setUp() {
        armyOne = new Army();
        armyOne.generateArmy();
        armyTwo = new Army();
        armyTwo.generateArmy();
        battle = new Battle(armyOne, armyTwo);
    }

    @Nested
    class AttackBonusTest {
        @Nested
        class Positive {
            @Test
            void attackBonusWithPlainsTerrain() {
                battle.setTerrain(Battle.Terrain.PLAINS);
                assertEquals(8, armyOne.getCavalryUnits().get(0).getAttackBonus(), "Attack bonus should be 8 with plains terrain");
            }
            @Test
            void attackBonusWithForestTerrain() {
                battle.setTerrain(Battle.Terrain.FOREST);
                assertEquals(6, armyOne.getCavalryUnits().get(0).getAttackBonus(), "Attack bonus should be 6 with forest terrain");
            }
            @Test
            void attackBonusWithLowRange() {
                armyOne.getCavalryUnits().get(0).attack(armyTwo.getInfantryUnits().get(0));
                armyOne.getCavalryUnits().get(0).attack(armyTwo.getInfantryUnits().get(0));
                assertEquals(2, armyOne.getCavalryUnits().get(0).getAttackBonus(), "Attack bonus should be 2 with low range");
            }
        }
    }

    @Nested
    class ResistBonusTest {
        @Nested
        class Positive {
            @Test
            void resistBonusWithForestTerrain() {
                battle.setTerrain(Battle.Terrain.FOREST);
                assertEquals(0, armyTwo.getCavalryUnits().get(0).getResistBonus(), "Resist bonus should be 0 with forest terrain");
            }
            @Test
            void resistBonusWithPlainsTerrain() {
                battle.setTerrain(Battle.Terrain.PLAINS);
                assertEquals(1, armyTwo.getCavalryUnits().get(0).getResistBonus(), "Resist bonus should be 1 with plains terrain");
            }
        }
    }
}