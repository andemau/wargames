package no.ntnu.idatt2001.wargames;

import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Units.CavalryUnit;
import no.ntnu.idatt2001.wargames.Units.CommanderUnit;
import no.ntnu.idatt2001.wargames.Units.InfantryUnit;
import no.ntnu.idatt2001.wargames.Units.RangedUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    Army armyOne;
    Army armyTwo;

    Battle battle;
    @BeforeEach
    void setUp() {
        armyOne = new Army();
        armyTwo = new Army();

        armyOne.addUnit(new InfantryUnit("infantry", 10));
        armyOne.addUnit(new CavalryUnit("cavalry", 20));
        armyOne.addUnit(new CommanderUnit("commander", 30));
        armyOne.addUnit(new RangedUnit("ranged", 15));
        armyOne.addUnit(new InfantryUnit("infantry", 10));
        armyOne.addUnit(new CavalryUnit("cavalry", 20));
        armyOne.addUnit(new CommanderUnit("commander", 30));
        armyOne.addUnit(new RangedUnit("ranged", 15));

        armyTwo.addUnit(new InfantryUnit("infantry", 10));
        armyTwo.addUnit(new CavalryUnit("cavalry", 20));
        armyTwo.addUnit(new CommanderUnit("commander", 30));
        armyTwo.addUnit(new RangedUnit("ranged", 15));

        battle = new Battle(armyOne, armyTwo);
        battle.setTerrain(Battle.Terrain.PLAINS);
    }

    @Nested
    class Constructor {
        @Nested
        class Negative {
            @Test
            void testNullArmyOne() {
                assertThrows(IllegalArgumentException.class, () -> new Battle(null, armyTwo), "Null armyOne should throw IllegalArgumentException");
            }
            @Test
            void testNullArmyTwo() {
                assertThrows(IllegalArgumentException.class, () -> new Battle(armyOne, null), "Null armyTwo should throw IllegalArgumentException");
            }
            @Test
            void testNullArmyOneAndTwo() {
                assertThrows(IllegalArgumentException.class, () -> new Battle(null, null), "Null armyOne and armyTwo should throw IllegalArgumentException");
            }
        }
    }

    @Nested
    class TestBattleSimulation {
        @Nested
        class Positive {
            @Test
            void simulateBattleTest() {
                battle.simulateBattle();
                assertEquals(armyOne, battle.getWinner(), "Army has more units and should win.");
            }

            @Test
            void resetBattleTest() {
                battle.simulateBattle();
                battle.resetBattle();
                assertNull(battle.getWinner(), "Winner should be null after battle is reset.");
            }
        }
    }
}