package no.ntnu.idatt2001.wargames.Army;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {
    @Nested
    class Constructor {
        @Nested
        class Negative {
            @Test
            void testConstructorWithEmptyName() {
                assertThrows(IllegalArgumentException.class, () -> new Army(""), "Name cannot be empty");
            }
        }
    }
}