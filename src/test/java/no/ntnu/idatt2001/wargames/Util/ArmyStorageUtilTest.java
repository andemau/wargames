package no.ntnu.idatt2001.wargames.Util;

import no.ntnu.idatt2001.wargames.Army.Army;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@IndicativeSentencesGeneration(separator = " -> ", generator =
        DisplayNameGenerator.ReplaceUnderscores.class)

class ArmyStorageUtilTest {

    private final ArmyStorageUtil armyStorageUtil = new ArmyStorageUtil();
    private static Army army;

    @BeforeAll
    static void setUp() {
        army = new Army();
        army.generateArmy();
    }

    @Nested
    class Can_save_army {
        @Nested
        class Positive {
            @Test
            void to_file_that_does_not_exist() throws IOException {
                Path path = Paths.get("src", "main",
                        "resources", "data", "fileNotExistsTest.csv");
                Files.deleteIfExists(path); //Cleanup files. Best way I can think of for now.
                armyStorageUtil.saveArmyToFile(army, "fileNotExistsTest");
                assertTrue(Files.exists(path));
            }
        }
        @Nested
        class Negative {
            @Test
            void to_file_that_exists() throws IOException {
                Path path = Paths.get("src", "main",
                        "resources", "data", "fileExistsTest.csv");
                Files.deleteIfExists(path);
                Files.createFile(path);
                assertThrows(IOException.class, () -> armyStorageUtil.saveArmyToFile(army, "fileExistsTest"));
            }
        }
    }

    @Nested
    class Can_load_army {
        @Nested
        class Positive {
            @Test
            void from_file_that_exists() throws IOException {
                Path path = Paths.get("src", "main",
                        "resources", "data", "loadFileExistsTest.csv");
                Files.deleteIfExists(path);
                armyStorageUtil.saveArmyToFile(army, "loadFileExistsTest");
                Army loadedArmy = armyStorageUtil.loadArmyFromFile("loadFileExistsTest");
                assertEquals(army, loadedArmy);
            }
        }
        @Nested
        class Negative {
            @Test
            void from_file_that_does_not_exist() {
                assertThrows(IOException.class, () -> armyStorageUtil.loadArmyFromFile("loadFileNotExistsTest"));
            }
        }
    }
}