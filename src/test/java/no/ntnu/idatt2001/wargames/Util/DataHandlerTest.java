package no.ntnu.idatt2001.wargames.Util;

import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.IndicativeSentencesGeneration;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@IndicativeSentencesGeneration(separator = " -> ", generator =
        DisplayNameGenerator.ReplaceUnderscores.class)
class DataHandlerTest {

    private final DataHandler dh = new DataHandler();
    final List<String> content = new ArrayList<>(
            Arrays.asList("field1", "field2", "field3", "field4")
    );

    @Nested
    class Can_write_to_file {
        @Nested
        class Positive {
            @Test
            void txt_file_that_does_not_exist(@TempDir Path tempDir) throws Exception{
                Path file = tempDir.resolve("data.txt");
                dh.writeToFile(file, content);
                assertAll("data",
                        () -> assertEquals(content.get(0), Files.readAllLines(file).get(0)),
                        () -> assertEquals(content.get(1), Files.readAllLines(file).get(1)),
                        () -> assertEquals(content.get(2), Files.readAllLines(file).get(2)),
                        () -> assertEquals(content.get(3), Files.readAllLines(file).get(3))
                );
            }

        }
        @Nested
        class Negative {
            @Test
            void txt_file_that_exists(@TempDir Path tempDir) throws IOException {
                Path file = tempDir.resolve("data.txt");
                Files.createFile(file);
                assertThrows(IOException.class, () -> dh.writeToFile(file, content));
            }
        }
    }

    @Nested
    class Can_read_from_file {
        @Nested
        class Positive {
            @Test
            void txt_file_that_exists(@TempDir Path tempDir) throws IOException {
                Path file = tempDir.resolve("data.txt");
                Files.write(file, content);
                assertAll("data",
                        () -> assertEquals(content.get(0), dh.readFile(file).get(0)),
                        () -> assertEquals(content.get(1), dh.readFile(file).get(1)),
                        () -> assertEquals(content.get(2), dh.readFile(file).get(2)),
                        () -> assertEquals(content.get(3), dh.readFile(file).get(3))
                );
            }
        }
        @Nested
        class Negative {
            @Test
            void txt_file_that_does_not_exist(@TempDir Path tempDir){
                Path file = tempDir.resolve("data.txt");
                assertThrows(IOException.class, () -> dh.readFile(file));
            }

        }
    }
}