package no.ntnu.idatt2001.wargames;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class WarGamesApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("WarGames Battle Simulator");

        Parent homeRoot = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/wargames-gui.fxml")));
        Scene homeScene = new Scene(homeRoot);

        primaryStage.setScene(homeScene);
        primaryStage.show();
    }

    @Override
    public void stop() {

    }
}
