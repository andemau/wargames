package no.ntnu.idatt2001.wargames.Army;

import no.ntnu.idatt2001.wargames.Units.*;
import no.ntnu.idatt2001.wargames.Util.UnitFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * The type Army.
 * Has fields for the army name, and a list of units.
 */
public class Army {
    private String name;
    private final List<Unit> units;
    private UnitFactory unitFactory = new UnitFactory();
    private List<Unit> backupUnits = new ArrayList<>();

    /**
     * Instantiates a new Army.
     *
     * @param name, name of the army
     */
    public Army(String name){
        if(Objects.equals(name, "") || name == null) throw new IllegalArgumentException("Name cannot be empty");
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * Instantiates a new Army.
     *
     * @param name, name of the army
     * @param units, a list of units to add to the army
     */
    public Army(String name, List<Unit> units) {
        if(Objects.equals(name, "") || name == null) throw new IllegalArgumentException("Name cannot be empty");
        if(units == null) throw new IllegalArgumentException("Unit list does not exist.");
        this.name = name;
        this.units = units;
    }

    /**
     * Instantiates an empty army.
     */
    public Army() {
        this.name = "";
        this.units = new ArrayList<>();
    }

    /**
     * Copy constructor, performs deep copy.
     *
     * @param army, army to copy
     */
    public Army(Army army) {
        this.name = army.name;
        this.units = new ArrayList<>(army.units);
        this.backupUnits = new ArrayList<>(army.backupUnits);
        this.unitFactory = new UnitFactory();
    }

    /**
     * Generates an army with a set number of units.
     * The generated army gets a randomly selected name from the list of names.
     */
    public void generateArmy() {
        List<String> names = new ArrayList<>(
                List.of("The Alliance",
                        "The Horde",
                        "The Undead",
                        "The Dark Elves",
                        "The Dwarven Army",
                        "Troll Army"));
        name = names.get((int) (Math.random() * names.size()));

        units.removeAll(units);

        int max = 7; //Sets the maximum interval for generation of units
        int min = 4; //Sets the minimum interval for generation of units
        for (int i = 0; i < (int) (Math.random() * (max - min) + min); i++) {
            units.add(new InfantryUnit("Swordsman", 10));
            units.add(new RangedUnit("Archer", 15));
            units.add(new CavalryUnit("Knight", 20));
            units.add(new CommanderUnit("Commander", 30));
        }
    }

    /**
     * Gets name.
     *
     * @return the name of the army
     */
    public String getName() {
        return name;
    }

    /**
     * Add unit.
     *
     * @param unit, unit to add to the army
     */
    public void addUnit(Unit unit) {
        units.add(unit);
    }

    /**
     * Add all units.
     *
     * @param units, list of units to add to the army
     */
    public void addAllUnits(List<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * Remove unit.
     *
     * @param unit, unit to remove from the army
     */
    public void removeUnit(Unit unit) {
        units.remove(unit);
    }

    /**
     * Has unit boolean.
     *
     * @param unit, unit to look for
     * @return boolean, true if the army has the unit
     */
    public boolean hasUnit(Unit unit) {
        return units.contains(unit);
    }

    /**
     * Gets all units.
     *
     * @return List of all units in the army
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Gets number of units.
     *
     * @return the number of units in the army
     */
    public int getNumberOfUnits() {
        return units.size();
    }

    /**
     * Gets random unit.
     *
     * @return a random unit from the army
     */
    public Unit getRandomUnit() {
        Random selector = new Random();
        return units.get(selector.nextInt(units.size()));
    }

    /**
     * Gets infantry units using lambda and stream methods.
     *
     * @return List containing the infantry units in the army
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(unit -> unit instanceof InfantryUnit).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    /**
     * Gets ranged units using lambda and stream methods.
     *
     * @return List containing the ranged units in the army
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(unit -> unit instanceof RangedUnit).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    /**
     * Gets cavalry units using lambda streams and methods.
     *
     * @return List containing the cavalry units in the army
     */
    public List<Unit> getCavalryUnits() {
        return units.stream().filter(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit)).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    /**
     * Gets commander units using lambda and stream methods.
     *
     * @return List containing the commander units in the army
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(unit -> unit instanceof CommanderUnit).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    /**
     * Populates a backup list of units, using a unitFactory to create
     * new units based on the units in the initial list.
     */
    public void setBackupUnits() {
        backupUnits.removeAll(backupUnits);
        for(Unit unit : units) {
            backupUnits.add(unitFactory.createUnit(unit.getClass().getSimpleName(), unit.getName(), unit.getHealth()));
        }
    }

    /**
     * Gets backup units.
     *
     * @return List of backup units
     */
    public List<Unit> getBackupUnits() {
        return backupUnits;
    }

    /**
     * Restores the backup units to the army.
     */
    public void restoreUnits() {
        for(Unit unit : backupUnits){
            units.add(unitFactory.createUnit(unit.getClass().getSimpleName(), unit.getName(), unit.getHealth()));
        }
    }

    @Override
    public String toString() {
        return "Name: " + name + "\n" + "Size of army: " + units.size() + " units."
                + "\n" + "Infantry units: " + getInfantryUnits().size() + "\n" + "Ranged units: " + getRangedUnits().size() +
                "\n" + "Cavalry units: " + getCavalryUnits().size() + "\n" + "Commander units: " + getCommanderUnits().size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Army army = (Army) o;

        return name.equals(army.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
