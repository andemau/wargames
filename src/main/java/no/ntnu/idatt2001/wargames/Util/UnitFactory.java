package no.ntnu.idatt2001.wargames.Util;

import no.ntnu.idatt2001.wargames.Units.*;

import java.util.ArrayList;
import java.util.List;

/**
 * UnitFactory is a factory class for creating units of different types.
 *
 * @author Anders
 */
public class UnitFactory{

    /**
     * Creates a unit of the specified type.
     * Switch statement is used to determine which unit to create,
     * using the unitType parameter.
     *
     * @param unitType Type of unit to create.
     * @param unitName Name of unit to create.
     * @param health   Unit health value.
     * @return A unit of the specified type, or null if the unitType is invalid.
     */
    public Unit createUnit(String unitType, String unitName, int health) {
        return switch (unitType.toLowerCase()) {
            case "infantryunit" -> new InfantryUnit(unitName, health);
            case "rangedunit" -> new RangedUnit(unitName, health);
            case "cavalryunit" -> new CavalryUnit(unitName, health);
            case "commanderunit" -> new CommanderUnit(unitName, health);
            default -> null;
        };
    }

    /**
     * Creates a list of numberOfUnits units of the specified type.
     * Uses the createUnit method to create each unit.
     *
     * @param unitType      Type of unit to create.
     * @param unitName      Name of unit to create.
     * @param health        Unit health value.
     * @param numberOfUnits Number of units to create.
     * @return unitList     A list of numberOfUnits units of the specified type.
     */
    public List<Unit> createUnitList(String unitType, String unitName, int health, int numberOfUnits) {
        List<Unit> unitList = new ArrayList<>();
        for(int i = 0; i < numberOfUnits; i++) {
            unitList.add(this.createUnit(unitType, unitName, health));
        }
        return unitList;
    }
}
