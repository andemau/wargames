package no.ntnu.idatt2001.wargames.Util;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

/**
 * DataHandler handles reading and writing to file.
 * Holds the path to the file as a String-variable.
 * @author Anders
 */
public class DataHandler {
    public DataHandler(){
    }

    /**
     * Gets a path for a file in the resources/data-folder
     *
     * @param fileName, fileName of the file to get
     * @param extension, file-extension
     * @return Path for fileName.extension in resources/data
     */
    public Path pathGetter(String fileName, String extension) {
        return Paths.get("src", "main",
                "resources", "data", fileName + "." + extension);
    }

    /**
     * Writes data from the list content to a new file created
     * at Path path.
     *
     * @param path, the Path to store the file at
     * @param content, a List containing strings to write to the file
     */
    public void writeToFile(Path path, List<String> content) throws IOException {
        Files.write(path, content, CREATE_NEW);
    }

    /**
     * Reads data from a file to a List of Strings.
     *
     * @param path, the path to find the file at.
     * @return List, a List of Strings containing the data from the file.
     */
    public List<String> readFile(Path path) throws IOException {
        return Files.readAllLines(path);
    }
}