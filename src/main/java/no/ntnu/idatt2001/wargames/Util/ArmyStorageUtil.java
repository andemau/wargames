package no.ntnu.idatt2001.wargames.Util;

import no.ntnu.idatt2001.wargames.Army.Army;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Storage utility for Army-class. Handles saving and loading of armies,
 * using DataHandler and UnitFactory utils to handle files and
 * create units.
 *
 * @author Anders
 */
public class ArmyStorageUtil {
    private final DataHandler dataHandler;
    private final UnitFactory unitFactory;


    /**
     * Constructor for ArmyStorageUtil.
     */
    public ArmyStorageUtil() {
        this.dataHandler = new DataHandler();
        this.unitFactory = new UnitFactory();
    }

    /**
     * Saves an Army-object's contents to a CSV-file.
     * The army's name and unit objects are stored as strings in the inputList,
     * and the inputList is written to fileName.csv using dataHandler's
     * writeToFile-method.
     *
     * @param army, the input Army-object to write to file.
     * @param fileName, the input fileName.
     */
    public void saveArmyToFile(Army army, String fileName) throws IOException {
        if(!Objects.equals(fileName, "") && !Objects.equals(army.getName(), "")) {
            List<String> inputList = new ArrayList<>();

            inputList.add(army.getName());
            army.getAllUnits().forEach(unit -> inputList.add(unit.csvFormattedString()));

            dataHandler.writeToFile(dataHandler.pathGetter(fileName, "csv"), inputList);
        } else if (Objects.equals(fileName, "")){
            throw new IllegalArgumentException("Filename cannot be empty.");
        } else if (Objects.equals(army.getName(), "")) {
            throw new IllegalArgumentException("No army to save. Generate an army.");
        }
    }


    /**
     * Instantiates an Army-object using data from a CSV-file.
     * Creates a List of Strings read from fileName.csv using
     * dataHandler's readFile-method, where the first element
     * in the list is the army name. The rest of the elements
     * in the list hold information for stored units.
     *
     * Units are added to the Army-object by splitting each
     * comma separated line in outputList and sending the values
     * as parameters for the unitFactory's createUnit-method.
     *
     * unitData[0] = unit type
     * unitData[1] = unit name
     * unitData[2] = unit health
     *
     * @param fileName, the name of the file to read.
     * @return Army, the Army-objected instantiated from the file.
     */
    public Army loadArmyFromFile(String fileName) throws IOException {
        if(!Objects.equals(fileName, "")) {
            List<String> outputList = dataHandler.readFile(dataHandler.pathGetter(fileName, "csv"));

            Army army = new Army(outputList.get(0));

            for (int i = 1; i < outputList.size(); ++i) {
                String[] unitData = outputList.get(i).split(",");
                army.addUnit(unitFactory.createUnit(unitData[0], unitData[1], Integer.parseInt(unitData[2])));
            }

            return army;
        } else {
            throw new IllegalArgumentException("Filename cannot be empty.");
        }
    }
}