package no.ntnu.idatt2001.wargames;

import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Units.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The type Battle.
 * Simulates a battle between two armies.
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private Army winner;
    private static Terrain terrain;
    private final Random randomizer = new Random();
    private final List<String> battleSimulationLines = new ArrayList<>();

    /**
     * Enum that holds the terrains a battle can be simulated with.
     */
    public enum Terrain {
        HILL, PLAINS, FOREST
    }

    /**
     * Instantiates a new Battle between two armies,
     * and sets the terrain.
     *
     * @param armyOne an Army object
     * @param armyTwo an Army object
     */
    public Battle(Army armyOne, Army armyTwo) {
        if(armyOne == null || armyTwo == null) throw new IllegalArgumentException("One or both armies are empty.");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }


    /**
     * Instantiates an empty Battle-object.
     */
    public Battle() {
        this.armyOne = null;
        this.armyTwo = null;
    }


    public void setArmyOne(Army armyOne) {
        if(armyOne == null) throw new IllegalArgumentException("The army is empty.");
        this.armyOne = armyOne;
    }

    public void setArmyTwo(Army armyTwo) {
        if(armyTwo == null) throw new IllegalArgumentException("The army is empty.");
        this.armyTwo = armyTwo;
    }
    /**
     * Get-method for the terrain.
     * Static method so it can be used to calculate Unit attack-bonus.
     * @return Terrain object
     */
    public static Terrain getTerrain() {
        return Battle.terrain;
    }

    /**
     * Set-method for the terrain.
     * @param terrain Terrain object
     */
    public void setTerrain(Terrain terrain) {
        if(terrain == null) throw new IllegalArgumentException("Terrain is null.");
        Battle.terrain = terrain;
    }

    /**
     * Gets the winner of the battle.
     * @return Army object
     */
    public Army getWinner() {
        return winner;
    }

    public Army getArmyOne() {
        return armyOne;
    }

    public Army getArmyTwo() {
        return armyTwo;
    }

    public List<String> getBattleSimulationLines() { return battleSimulationLines;}

    /**
     * Simulates a battle between two armies.
     * As long as there are units left in both armies, the battle continues.
     * When one army is empty, the other army wins.
     */
    public void simulateBattle() {
        if(armyOne != null && armyTwo != null) {
            while(armyOne.getNumberOfUnits() > 0 && armyTwo.getNumberOfUnits() > 0) {
                simulateStrike();

                if (armyOne.getNumberOfUnits() == 0) {
                    this.winner = armyTwo;
                    battleSimulationLines.add(armyTwo.getName() + " has defeated " + armyOne.getName() + " with " +
                            armyOne.getNumberOfUnits() + " units remaining in the battlefield.");
                    break;
                } else if (armyTwo.getNumberOfUnits() == 0) {
                    this.winner = armyOne;
                    battleSimulationLines.add(armyOne.getName() + " has defeated " + armyTwo.getName() + " with " +
                            armyTwo.getNumberOfUnits() + " units remaining in the battlefield.");
                    break;
                } else if (armyOne.getNumberOfUnits() == 0 && armyTwo.getNumberOfUnits() ==0 ) {
                    battleSimulationLines.add("The battle was a draw!");
                }
            }
        } else {
            throw new IllegalArgumentException("One or both armies are empty. Please load an army, or generate a new army.");
        }
    }


    /**
     * Simulates one strike between two units.
     * Uses randomizer for random order of attack.
     */
    private void simulateStrike() {
        Unit unitOne = armyOne.getRandomUnit();
        int unitOneHealthPreStrike = unitOne.getHealth();

        Unit unitTwo = armyTwo.getRandomUnit();
        int unitTwoHealthPreStrike = unitTwo.getHealth();

        if(randomizer.nextInt(101) < 50) {
            attack(unitOne, unitOneHealthPreStrike, unitTwo, unitTwoHealthPreStrike);
        } else if (randomizer.nextInt(101) >= 50) {
            attack(unitTwo, unitTwoHealthPreStrike, unitOne, unitOneHealthPreStrike);
        }

        if(unitOne.getHealth() == 0) {
            battleSimulationLines.add(unitOne.getName() + " has died.");
            armyOne.getAllUnits().remove(unitOne);
        }

        if(unitTwo.getHealth() == 0) {
            battleSimulationLines.add(unitTwo.getName() + " has died.");
            armyTwo.getAllUnits().remove(unitTwo);
        }
    }

    /**
     * Helper method for simulating strikes between two Units.
     * Avoids code repetition.
     * Appends strings that detail the result of a strike to
     * battleSimulationLines, used to display battle simulation
     * in real-time.
     *
     * @param unitOne, unit from armyOne
     * @param unitOneHealthPreStrike, unit's health pre-strike, used to calculate damage dealt
     * @param unitTwo, unit from armyTwo
     * @param unitTwoHealthPreStrike, unit's health pre-strike, used to calculate damage dealt
     */
    private void attack(Unit unitOne, int unitOneHealthPreStrike, Unit unitTwo, int unitTwoHealthPreStrike) {
        unitOne.attack(unitTwo);
        battleSimulationLines.add(unitOne.getName() + " from " + armyOne.getName() + " attacked " + unitTwo.getName() +
               " from " + armyTwo.getName() + " for " + (unitTwoHealthPreStrike - unitTwo.getHealth()) + " damage.");
        unitTwo.attack(unitOne);
        battleSimulationLines.add(unitTwo.getName() + " from " + armyTwo.getName() + " attacked " + unitOne.getName() +
                " from " + armyOne.getName() + " for " + (unitOneHealthPreStrike - unitOne.getHealth()) + " damage.");
    }


    /**
     * Method to reset a battle to its initial state, by reviving all units
     * and setting winner to null.
     */
    public void resetBattle() {
        if(armyOne != null && armyTwo != null) {
            if (armyOne.getAllUnits().size() < armyOne.getBackupUnits().size()
                    && armyTwo.getAllUnits().size() < armyTwo.getBackupUnits().size()) {
                armyOne.getAllUnits().removeAll(armyOne.getAllUnits());
                armyTwo.getAllUnits().removeAll(armyTwo.getAllUnits());
                armyOne.restoreUnits();
                armyTwo.restoreUnits();
            }
            battleSimulationLines.removeAll(battleSimulationLines);
            this.winner = null;
        } else {
            throw new IllegalArgumentException("One or both armies are empty. Please load an army, or generate a new army.");
        }
    }
    @Override
    public String toString() {
        return "Battle:" + armyOne + " vs " + armyTwo + ".\n" + armyOne.getName() + " has :"
                + armyOne.getNumberOfUnits() + " units left.\n" + armyTwo.getName()
                + " has :" + armyTwo.getNumberOfUnits() + " units left-";
    }
}
