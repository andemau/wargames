package no.ntnu.idatt2001.wargames.Controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import no.ntnu.idatt2001.wargames.Army.Army;
import no.ntnu.idatt2001.wargames.Battle;

import no.ntnu.idatt2001.wargames.Util.ArmyStorageUtil;

import java.io.IOException;

public class GUIController{
    private Battle battle;
    private final ArmyStorageUtil armyStorageUtil = new ArmyStorageUtil();

    ObservableList<Battle.Terrain> battleTerrains = FXCollections.observableArrayList(Battle.Terrain.values());
    ObservableList<String> unitTypes = FXCollections.observableArrayList("InfantryUnit", "RangedUnit", "CavalryUnit", "CommanderUnit");

    @FXML
    private TextArea armyOneTextArea;

    @FXML
    private TextArea armyTwoTextArea;

    @FXML
    private ChoiceBox<String> armyOneUnitSelector;

    @FXML
    private ChoiceBox<String> armyTwoUnitSelector;

    @FXML
    private ChoiceBox<Battle.Terrain> battleTerrainSelect;

    @FXML
    private TextArea armyOneUnitTextArea;

    @FXML
    private TextArea armyTwoUnitTextArea;

    @FXML
    private TextField armyOneFileTextField;

    @FXML
    private TextField armyTwoFileTextField;

    @FXML
    private TextArea battleSimTextArea;

    private boolean reset = false;


    /**
     * Initializes the GUI Controller/main window.
     * Creates a new Battle-object containing two armies.
     * Adds items to ChoiceBoxes and sets their onAction-methods.
     * Also initializes alert messages.
     **/
    @FXML
    private void initialize(){
        this.battle = new Battle();
        battleTerrainSelect.setItems(battleTerrains);
        armyOneUnitSelector.setItems(unitTypes);
        armyTwoUnitSelector.setItems(unitTypes);

        armyOneUnitSelector.setOnAction(e -> unitSwitcher(battle.getArmyOne(), armyOneUnitSelector, armyOneUnitTextArea));
        armyTwoUnitSelector.setOnAction(e -> unitSwitcher(battle.getArmyTwo(), armyTwoUnitSelector, armyTwoUnitTextArea));

        battleTerrainSelect.setOnAction(e -> battle.setTerrain(battleTerrainSelect.getValue()));
    }

    /**
     * Gets a stream from the army-object's list of units and filters it
     * by the unit-type selected in the ChoiceBox.
     * Appends each filtered unit's toString to a StringBuilder.
     * Sets the TextArea to the StringBuilder's toString.
     *
     *
     * @param army The army to get the units from.
     * @param unitSelector The ChoiceBox containing the unit-type to filter by.
     * @param unitTextArea The TextArea to display the filtered units.
     */
    private void unitSwitcher(Army army, ChoiceBox<String> unitSelector, TextArea unitTextArea){
        StringBuilder unitListString = new StringBuilder();
        army.getAllUnits().stream()
                .filter(unit -> unit.getClass().getSimpleName().equals(unitSelector.getValue()))
                .forEach(unit -> unitListString.append(unit).append("\n"));
        unitTextArea.setText(unitListString.toString());
    }

    /**
     * Method that is called when army one's generate button is pressed.
     * Uses the generation helper method to generate the army and set
     * relevant text fields.
     **/
    public void generateArmyOne(){
        battle.setArmyOne(generateArmy(armyOneTextArea));
    }

    /**
     * Method that is called when army one's generate button is pressed.
     * Uses the generation helper method to generate the army and set
     * relevant text fields.
     **/
    public void generateArmyTwo(){
        battle.setArmyTwo(generateArmy(armyTwoTextArea));
    }

    /**
     * Helper method that takes care of generating armies.
     * Creates a new army-object and sets the text fields to the army's
     * toString.
     * @param textArea, the TextArea that will display the army's toString.
     * @return the generated army.
     */
    private Army generateArmy(TextArea textArea) {
        Army army = new Army();
        army.generateArmy();
        army.setBackupUnits();
        textArea.setText(army.toString());
        return army;
    }
    /**
     * Method to run a battle simulation.
     * Will be changed to implement real-time log-style output of battle.
     **/
    public void simulateBattle(){
        try {
            reset = false;
            battle.simulateBattle();
            battleSimTextAreaRunner();
        } catch (Exception e) {
            battleSimTextArea.setText(e.getMessage());
        }
    }

    /**
     * Method for army one's load button.
     * Passes required information to the helper method
     * that takes care of loading the army from file.
     **/
    public void loadArmyOneFromFile(){
        try {
            battle.setArmyOne(loadArmyFromFile(armyOneFileTextField));
            armyOneTextArea.setText(battle.getArmyOne().toString());
        } catch (Exception e) {
            armyOneTextArea.setText(e.getMessage());
        }
    }

    /**
     * Method for army one's load button.
     * Passes required information to the helper method
     * that takes care of loading the army from file.
     **/
    public void loadArmyTwoFromFile(){
        try {
            battle.setArmyTwo(loadArmyFromFile(armyTwoFileTextField));
            armyTwoTextArea.setText(battle.getArmyTwo().toString());
        } catch (Exception e) {
            armyTwoTextArea.setText(e.getMessage());
        }
    }

    /**
     * Helper method that uses an input text field to load an army from file.
     * Creates a new army using the armyStorageUtil object with the file path
     * gotten from the text field.
     * Sets the text area to the army's toString.
     *
     * @param filePath, the TextField that contains the file path.
     * @return the loaded army.
     */
    private Army loadArmyFromFile(TextField filePath) throws IOException {
        Army army = new Army(armyStorageUtil.loadArmyFromFile(filePath.getText()));
        army.setBackupUnits();
        return army;
    }

    /**
     * Method for army one's save button.
     * Passes required information to the helper method
     * that takes care of saving the army to file.
     **/
    public void saveArmyOneToFile(){
        try {
            saveArmy(battle.getArmyOne(), armyOneFileTextField);
            armyOneTextArea.setText(battle.getArmyOne().toString()); //Refresh text area in case of error messages
        } catch (Exception e) {
            armyOneTextArea.setText(e.getMessage());
        }
    }

    /**
     * Method for army one's save button.
     * Passes required information to the helper method
     * that takes care of saving the army to file.
     **/
    public void saveArmyTwoToFile(){
        try {
            saveArmy(battle.getArmyTwo(), armyTwoFileTextField);
            armyTwoTextArea.setText(battle.getArmyTwo().toString()); //Refresh text area in case of error messages.
        } catch (Exception e) {
            armyTwoTextArea.setText(e.getMessage());
        }
    }

    /**
     * Helper method that uses an input text field to save an input army object to file.
     * Saves the army using the armyStorageUtil object with the file path gotten from the text field.
     * Sets the text area to the army's toString, to refresh the view in case exceptions are caught
     * and displayed.
     * @param army, the army to save.
     * @param filePath, the TextField that contains the file path.
     */
    private void saveArmy(Army army, TextField filePath) throws IOException{
        armyStorageUtil.saveArmyToFile(army, filePath.getText());
        Alert saveAlert = new Alert(Alert.AlertType.INFORMATION);
        saveAlert.setTitle("Saved");
        saveAlert.setContentText("Your army has been saved");
        saveAlert.show();
    }


    /**
     * Adds functionality to top-menu exit button.
     */
    public void menuBarQuit() {
        Platform.exit();
    }

    /**
     * Method to reset the battle.
     * Resets the battle to its initial state.
     **/
    public void resetBattle() {
        try {
            reset = true;
            battle.resetBattle();
            battleSimTextArea.setText("");
            armyOneTextArea.setText(battle.getArmyOne().toString());
            armyTwoTextArea.setText(battle.getArmyTwo().toString());
        } catch (Exception e) {
            battleSimTextArea.setText(e.getMessage());
        }
    }

    /**
     * Method to run the real-time display of the battle simulation.
     * Creates a new thread, separate from the JavaFX main thread,
     * so that the text display can sleep independently of the
     * main window. A Runnable-object appends text to the
     * battle simulation text area, fetching text from battle's
     * List holding the generated lines.
     *
     * Based on example from riptutorial.com/javafx/example/7291/updating-the-ui-using-platform-runlater
     */
    private void battleSimTextAreaRunner(){
        final int[] incr = {0};
        battleSimTextArea.setText("");
        Thread thread = new Thread(() -> {
            Runnable updater = () -> {
                battleSimTextArea.appendText(battle.getBattleSimulationLines().get(incr[0]) + "\n\n");
                ++incr[0];
            };

            while (true) {
                try {
                    Thread.sleep(500);
                    if(incr[0] == battle.getBattleSimulationLines().size()) {
                        break;
                    }
                    if(reset) {
                        break;
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                // UI update is run on the Application thread
                Platform.runLater(updater);
            }
        });

        thread.setDaemon(true);
        thread.start();

        armyOneTextArea.setText(battle.getArmyOne().toString());
        armyTwoTextArea.setText(battle.getArmyTwo().toString());
    }
}
