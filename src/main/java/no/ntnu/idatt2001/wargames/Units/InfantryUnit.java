package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Battle;
import no.ntnu.idatt2001.wargames.Battle.Terrain;

/**
 * The type Infantry unit.
 */
public class InfantryUnit extends Unit {
    /**
     * Instantiates a new Infantry unit.
     *
     * @param name   the name of the unit
     * @param health the health value of the unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * InfantryUnit has higher attack bonus if the terrain is forest.
     * @return attack bonus value
     */
    @Override
    public int getAttackBonus() {
        if(Battle.getTerrain() == Terrain.FOREST) {
            return 5;
        }
        return 2;
    }

    @Override
    public int getResistBonus() {
        if(Battle.getTerrain() == Terrain.FOREST) {
            return 5;
        }
        return 1;
    }
}
