package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Battle;

/**
 * The type Ranged unit.
 */
public class RangedUnit extends Unit {
    /**
     * Instantiates a new Ranged unit.
     *
     * @param name   the name of the unit
     * @param health the health value of the unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * RangedUnit gets higher attack bonus if the terrain is hill or forest.
     * @return bonus-value.
     */
    @Override
    public int getAttackBonus(){
        if(Battle.getTerrain() == Battle.Terrain.HILL) {
            return 7;
        } else if(Battle.getTerrain() == Battle.Terrain.FOREST) {
            return 5;
        }
        return 3;
    }

    /**Ranged units get higher damage resistance at range, decreasing for each attack as the attacker gets closer,
     * represented by the return values.
     **/
    @Override
    public int getResistBonus(){
        if(getRange() == 3) {
            return 6;
        } else if (getRange() == 2) {
            return 4;
        } else {
            return 2;
        }
    }
}
