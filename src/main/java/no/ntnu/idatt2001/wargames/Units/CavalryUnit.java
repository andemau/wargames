package no.ntnu.idatt2001.wargames.Units;

import no.ntnu.idatt2001.wargames.Battle;

/**
 * The type Cavalry unit.
 */
public class CavalryUnit extends Unit {

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name of the unit
     * @param health health value of the unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * CavalryUnit has higher attack bonus if range is bigger than 1.
     * Also gets a bonus if terrain is plains.
     * @return attack bonus value.
     */
    @Override
    public int getAttackBonus() {
        int bonus = 2;
        if(getRange() > 1){
            bonus = 6;
        }
        if(Battle.getTerrain() == Battle.Terrain.PLAINS){
            bonus += 2;
        }
        return bonus;
    }

    /**
     * CavalryUnit has lower resist bonus if terrain is forest.
     * @return resist bonus value
     */
    @Override
    public int getResistBonus() {
        if(Battle.getTerrain() == Battle.Terrain.FOREST){
            return 0;
        }
        return 1;
    }
}
