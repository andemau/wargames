package no.ntnu.idatt2001.wargames.Units;

/**
 * The type Commander unit.
 */
public class CommanderUnit extends CavalryUnit {
    /**
     * Instantiates a new Commander unit.
     *
     * @param name   the name of the unit
     * @param health the health value of the unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }
}
