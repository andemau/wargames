package no.ntnu.idatt2001.wargames.Units;

import java.util.Objects;

/**
 * The type Unit.
 */
public abstract class Unit {
    private final String name;
    private int health;
    private final int attack;
    private final int armor;
    private int range = 3;

    /**
     * Instantiates a new Unit.
     *
     * @param name   the name of the unit
     * @param health the health value of the unit
     * @param attack the attack value of the unit
     * @param armor  the armor value of the unit
     */
    public Unit(String name, int health, int attack, int armor) {
        if(health < 0 || attack < 0 || armor < 0)  throw new IllegalArgumentException("Health, attack and armor must be positive values.");
        if(Objects.equals(name, "") || name == null) throw new IllegalArgumentException("Name cannot be empty.");
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Gets name.
     *
     * @return unit name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health.
     *
     * @return int  unit health value
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets attack.
     *
     * @return int  unit attack value
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor.
     *
     * @return int  unit armor value
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets health.
     *
     * @param health, new unit health value
     */
    public void setHealth(int health) {
        if(this.health > this.health - health) {
            this.health = health;
        } else {
            this.health = 0;
        }
    }

    /**
     * Gets range.
     *
     * @return int  unit range value
     */
    public int getRange() {
        return range;
    }

    // Helper method for controlling range-based bonuses for different units.
    //Decrements range by 1 for each attack, simulating units moving closer to the target.
    private void setRange() {
        if(this.range > 0) {
            --this.range;
        }
    }

    /**
     * Gets attack bonus.
     *
     * @return int   attack bonus value
     */
    public abstract int getAttackBonus();

    /**
     * Gets resist bonus.
     *
     * @return int   resist bonus
     */
    public abstract int getResistBonus();

    /**
     * Attack method for units. Takes a target unit and reduces its health by the attack value formula provided.
     * Uses helper method setRange() to simulate units moving closer to eachother, one "step" per attack.
     *
     * @param target, the Unit-object to target.
     */
    public void attack(Unit target) {
        target.setHealth(target.getHealth() - (this.attack + this.getAttackBonus()) + (target.getArmor() + target.getResistBonus()));
        target.setRange();
        this.setRange();
    }

    @Override
    public String toString() {
        return "Name: " + name + "\nHealth: " + health + "\nAttack: " + attack + "\nArmor: " + armor + "\n";
    }

    /**
     * Creates a String representation of a Unit object, formatted for CSV-file output.
     *
     * @return CSV-formatted String.
     */
    public String csvFormattedString() {
        return this.getClass().getSimpleName() + "," + name + "," + health;
    }
}
