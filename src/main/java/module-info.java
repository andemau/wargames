module no.ntnu.idatt2001.wargames {
    requires transitive javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    exports no.ntnu.idatt2001.wargames;
    exports no.ntnu.idatt2001.wargames.Units;
    exports no.ntnu.idatt2001.wargames.Army;
    exports no.ntnu.idatt2001.wargames.Util;

    opens no.ntnu.idatt2001.wargames.Util to javafx.fxml;
    exports no.ntnu.idatt2001.wargames.Controllers;
    opens no.ntnu.idatt2001.wargames.Controllers to javafx.fxml;
}